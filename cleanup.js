const rmUnusedCss = require('rm-unused-css');
const cssSource = 'styles.css';
rmUnusedCss(cssSource, { path: 'index.html', override: true }).then((result) => {
    console.log(result.file, result.newContent)
});